const rightDiv = document.getElementsByClassName("rightDiv");
const leftDiv = document.getElementsByClassName("leftDiv");

const imgLinks = [
  "https://arounda.agency/assets/images/index/wordpress.jpg",
  "https://arounda.agency/assets/images/index/mintyswap.jpg",
  "https://arounda.agency/assets/images/index/qtalent.jpg",
];

let image = document.getElementsByTagName("img");

// console.log(image);
// let src = image[0].currentSrc;

// console.log(rightDiv);

const circle = rightDiv[0].children[1];
const heading = leftDiv[0];

// console.log(heading);

// console.log(circle);

window.onscroll = async function () {
  let Y = heading.getBoundingClientRect().top;
  console.log(Y);
  if (Y < -120) {
    rightDiv[0].classList.add("changeColor");
  } else {
    if (rightDiv[0].classList.contains("changeColor")) {
      rightDiv[0].classList.replace("changeColor", "changeColor2");
    }
  }
};
